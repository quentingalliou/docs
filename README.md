# HOW-TO AppSec

# Metrics

### WRT (Weighted Risk Trend)

**Measure**: Issues

**KPI**: Product owners

**How to calculate**: (BUG1_severity + BUG2_severity ...) x product business criticality

**Example**:
```
Low = 5
Medium = 10
High = 25
Critical = 50
(Low + Low + Medium) * 50% = (5 + 5 + 10) * 50% = 10
```

### Time to triage

**Measure**: Issues

**KPI**: Security team

**How to calculate**: Date triaged - Date created

**Example**:
```
10.10.2015 - 01.10.2015 = 9
```

### Time to fix

**Measure**: Issues

**KPI**: Product owners

**How to calculate**: Date fixed - Date triaged

**Example**:
```
20.10.2015 - 10.10.2015 = 10
```

### False positive rate

**Measure**: Scanners

**KPI**: Security team

**How to calculate**: (False positive count / Total findings count) * 100

**Example**:
```
(40 / 80) * 100 = 50%
```

### Security coverage

**Measure**: Integrated projects

**KPI**: R&D team

**How to calculate**: (Integrated projects count / total projects count) * 100

**Example**:
```
(40 / 80) * 100 = 50%
```

### Educated employees count
To be published

# Basic threat assessment

### Prepare scored questionaire 

To be published


# Security requirements

Use OWASP ASVS to get all requirements or https://requirements.whitespots.io (https://github.com/Whitespots-OU/security-requirements-generator) if you need only specific ones

# DevSecOps

0. Deploy DefectDojo
1. Clone `pipelines` repo from this group and push to your corporate gitlab
2. Edit `SEC_DD_URL` variable in `common/variables.yml` file 
3. Add `SEC_DD_URL` variable in CI/CD variables of your developers group (for example `whitespots-public` here. You need to set up that variable in all dev groups)
4. Edit `.tags` section in `pipelines.yml` with your own value
5. Clone `security-images` repo from this group and push to your corporate gitlab
6. Run build stage in `security-images` repository pipeline
7. Done, just use integration examples
